package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/api"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ListenAddress string
	PostgresDSN   string
	Municipality  string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "sdg-listen-address", "", "0.0.0.0:8080", "Address for the api to listen on.")
	serveCommand.Flags().StringVarP(&serveOpts.PostgresDSN, "sdg-postgres-dsn", "", "", "Postgres Connection URL")

	// Required flags
	if err := serveCommand.MarkFlagRequired("sdg-postgres-dsn"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		err := storage.PostgresPerformMigrations(serveOpts.PostgresDSN)
		if err != nil {
			log.Fatalf("failed to migrate db: %v", err)
		}

		db, err := storage.New(serveOpts.PostgresDSN)
		if err != nil {
			log.Fatalf("failed to connect to the database: %v", err)
		}

		apiArgs := &api.NewAPIArgs{
			DB:            db,
			ListenAddress: serveOpts.ListenAddress,
		}

		a, err := api.New(apiArgs)
		if err != nil {
			log.Fatalf("failed to setup api: %v", err)
		}

		if err := a.ListenAndServe(); err != nil {
			log.Fatalf("failed to listen and serve: %v", err)
		}
	},
}
