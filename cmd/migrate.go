package cmd

import (
	"log"
	"os"

	_ "github.com/golang-migrate/migrate/v4/database/postgres" // database driver
	_ "github.com/lib/pq"                                      // database driver
	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage"
)

var migrateOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	PostgresDSN string
}

func init() { //nolint:gochecknoinits // this is the recommended way to use cobra
	migrateUpCommand.Flags().StringVarP(&migrateOpts.PostgresDSN, "sdg-postgres-dsn", "", "", "Postgres Connection URL")

	if err := migrateUpCommand.MarkFlagRequired("sdg-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	migrateCommand.AddCommand(migrateUpCommand)
}

var migrateCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "migrate",
	Short: "Run the migration tool",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

var migrateUpCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "up",
	Short: "Up the migrations",
	Run: func(cmd *cobra.Command, args []string) {
		err := storage.PostgresPerformMigrations(migrateOpts.PostgresDSN)
		if err != nil {
			log.Fatal(err)
		}

		os.Exit(0)
	},
}
