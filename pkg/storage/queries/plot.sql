-- name: PlotAttach :exec
INSERT INTO digilab_demo_frag.building_plot (
    building_id, plot_id
)
VALUES ($1, $2);

-- name: PlotBuildings :many
SELECT plot_id
FROM digilab_demo_frag.building_plot
WHERE building_id=$1;
