-- name: BuildingListAsc :many
SELECT b.id, b.constructed_at, b.surface, b.internal_id FROM (
    SELECT id, constructed_at, surface, internal_id
    FROM digilab_demo_frag.buildings
    WHERE buildings.internal_id > (
        SELECT internal_id
        FROM digilab_demo_frag.buildings as sub_qry_buildings
        WHERE sub_qry_buildings.id = $1
    )
    ORDER BY buildings.internal_id ASC
    LIMIT $2
) b ORDER BY b.internal_id DESC;

-- name: BuildingListDesc :many
SELECT id, constructed_at, surface, internal_id
FROM digilab_demo_frag.buildings
WHERE buildings.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_frag.buildings as sub_qry_buildings
    WHERE sub_qry_buildings.id = $1
), $2)
ORDER BY buildings.internal_id DESC
LIMIT $3;

-- name: BuildingGet :one
SELECT id, constructed_at, surface, internal_id
FROM digilab_demo_frag.buildings
WHERE id=$1;

-- name: BuildingAddress :many
SELECT buildings.id, buildings.constructed_at, buildings.surface, buildings.internal_id
FROM digilab_demo_frag.buildings as buildings
LEFT JOIN digilab_demo_frag.address_building as addresses
    ON addresses.address_id=$1
WHERE buildings.id=addresses.building_id;

-- name: BuildingCreate :exec
INSERT INTO digilab_demo_frag.buildings (
    id, constructed_at, surface
)
VALUES ($1, $2, $3);

-- name: BuildingUpdate :exec
UPDATE digilab_demo_frag.buildings
SET constructed_at=$1, surface=$2
WHERE id=$3;

-- name: BuildingDelete :exec
DELETE FROM digilab_demo_frag.buildings
WHERE id=$1;

-- name: BuildingAttach :exec
INSERT INTO digilab_demo_frag.address_building (
    building_id, address_id
)
VALUES ($1, $2);
