-- name: AddressListAsc :many
SELECT a.id, a.street, a.zip_code,a.municipality_code, a.municipality_name, a.purpose, a.surface, a.house_number, a.house_number_addition, a.internal_id FROM (
    SELECT id, street, zip_code, municipality_code, municipality_name, purpose, surface, house_number, house_number_addition, internal_id
    FROM digilab_demo_frag.addresses, plainto_tsquery($3) q
    WHERE addresses.internal_id > (
        SELECT internal_id
        FROM digilab_demo_frag.addresses as sub_qry_addresses
        WHERE sub_qry_addresses.id = $1
    ) AND (($3 = '') OR tsv @@ q)
    ORDER BY internal_id ASC
    LIMIT $2
) a ORDER BY a.internal_id DESC;

-- name: AddressListDesc :many
SELECT id, street, zip_code, municipality_code, municipality_name, purpose, surface, house_number, house_number_addition, internal_id
FROM digilab_demo_frag.addresses, to_tsquery($4) q
WHERE addresses.internal_id < COALESCE((
    SELECT internal_id
    FROM digilab_demo_frag.addresses as sub_qry_addresses
    WHERE sub_qry_addresses.id = $1
), $2) AND (($4 = '') OR tsv @@ q)
ORDER BY ts_rank(tsv, q) desc, addresses.internal_id DESC
LIMIT $3;

-- name: AddressGet :one
SELECT id, street, zip_code, municipality_code, municipality_name, purpose, surface, house_number, house_number_addition, internal_id
FROM digilab_demo_frag.addresses
WHERE id=$1;

-- name: AddressGetMany :many
SELECT id, street, zip_code, municipality_code, municipality_name, purpose, surface, house_number, house_number_addition, internal_id
FROM digilab_demo_frag.addresses
WHERE id = ANY($1::uuid[]);

-- name: AddressBuilding :many
SELECT addresses.id, addresses.street,
    addresses.zip_code, addresses.municipality_code, addresses.municipality_name, addresses.purpose, addresses.surface, addresses.house_number, addresses.house_number_addition, addresses.internal_id
FROM digilab_demo_frag.addresses as addresses
LEFT JOIN digilab_demo_frag.address_building as buildings
    ON buildings.building_id=$1
WHERE addresses.id=buildings.address_id;

-- name: AddressCreate :exec
INSERT INTO digilab_demo_frag.addresses (
    id, street, zip_code, municipality_code, municipality_name, purpose, surface, house_number, house_number_addition
)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);

-- name: AddressUpdate :exec
UPDATE digilab_demo_frag.addresses
SET street = $1, house_number = $7, house_number_addition = $8, zip_code = $2, municipality_code = $3, municipality_name = $4, purpose = $5, surface = $6
WHERE id = $9;

-- name: AddressDelete :exec
DELETE FROM digilab_demo_frag.addresses
WHERE id=$1;
