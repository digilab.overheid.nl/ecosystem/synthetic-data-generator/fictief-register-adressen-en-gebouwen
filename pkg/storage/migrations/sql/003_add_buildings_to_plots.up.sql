BEGIN TRANSACTION;

ALTER TABLE digilab_demo_frag.buildings DROP COLUMN plot_id;

CREATE TABLE digilab_demo_frag.building_plot (
    building_id UUID NOT NULL,
    plot_id UUID NOT NULL,

    PRIMARY KEY(building_id, plot_id),

    CONSTRAINT fk_building_plot_building_id FOREIGN KEY(building_id) REFERENCES digilab_demo_frag.buildings(id)
);

COMMIT;
