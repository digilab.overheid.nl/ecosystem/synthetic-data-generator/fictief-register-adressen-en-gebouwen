BEGIN transaction;

CREATE SCHEMA IF NOT EXISTS digilab_demo_frag;

CREATE TABLE digilab_demo_frag.buildings (
    id UUID NOT NULL,
    plot_id UUID,
    constructed_at DATE,
    surface INT,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frag.addresses (
    id UUID NOT NULL,
    street TEXT,
    zip_code TEXT,
    municipality TEXT,
    purpose TEXT,
    surface INT,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frag.address_building (
    address_id UUID NOT NULL,
    building_id UUID NOT NULL,

    PRIMARY KEY(address_id, building_id),

    CONSTRAINT fk_address_building_address_id FOREIGN KEY(address_id) REFERENCES digilab_demo_frag.addresses(id),
    CONSTRAINT fk_address_building_buiding_id FOREIGN KEY(building_id) REFERENCES digilab_demo_frag.buildings(id)
);

CREATE INDEX idx_buildings_plot_id ON digilab_demo_frag.buildings(plot_id);

COMMIT;
