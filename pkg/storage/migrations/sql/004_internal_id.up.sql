BEGIN TRANSACTION;

ALTER TABLE digilab_demo_frag.addresses
ADD COLUMN internal_id SERIAL NOT NULL;

ALTER TABLE digilab_demo_frag.buildings
ADD COLUMN internal_id SERIAL NOT NULL;

COMMIT;
