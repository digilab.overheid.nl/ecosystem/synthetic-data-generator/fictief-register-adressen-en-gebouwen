ALTER TABLE digilab_demo_frag.addresses ADD COLUMN tsv tsvector;
UPDATE digilab_demo_frag.addresses SET tsv =
setweight(to_tsvector(street), 'A') ||
setweight(to_tsvector(zip_code), 'B') ||
setweight(to_tsvector(municipality), 'B') ||
setweight(to_tsvector(house_number::text), 'B') ||
setweight(to_tsvector(house_number_addition), 'B') ||
setweight(to_tsvector(id::text), 'C');

CREATE INDEX ix_addresses_tsv ON digilab_demo_frag.addresses USING GIN(tsv);
