BEGIN TRANSACTION;

ALTER TABLE digilab_demo_frag.addresses ADD COLUMN house_number INT;
ALTER TABLE digilab_demo_frag.addresses ADD COLUMN house_number_addition TEXT;

COMMIT;
