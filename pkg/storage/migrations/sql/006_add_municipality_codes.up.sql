ALTER TABLE digilab_demo_frag.addresses ADD COLUMN municipality_code TEXT;
ALTER TABLE digilab_demo_frag.addresses RENAME COLUMN municipality TO municipality_name;

UPDATE digilab_demo_frag.addresses SET municipality_code = '0999' WHERE municipality_name = 'Woudendijk';
UPDATE digilab_demo_frag.addresses SET municipality_code = '0998' WHERE municipality_name = 'Zonnendaal';
