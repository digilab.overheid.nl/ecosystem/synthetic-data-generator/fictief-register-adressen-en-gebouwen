package migrations

import (
	"embed"
	"fmt"
	"net/http"

	"github.com/golang-migrate/migrate/v4/source"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
)

//go:embed sql/*.sql
var migrations embed.FS

func RegisterDriver(driverName string) {
	source.Register(driverName, new(driver))
}

type driver struct {
	httpfs.PartialDriver
}

func (d *driver) Open(_ string) (source.Driver, error) { //nolint:ireturn // is necessary to satisfy interface
	err := d.PartialDriver.Init(http.FS(migrations), "sql")
	if err != nil {
		return nil, fmt.Errorf("open failed: %w", err)
	}

	return d, nil
}
