package adapter

import (
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/queries/generated"
)

func ToAddress(record *queries.AddressGetRow) (*model.Address, error) {
	return &model.Address{
		ID:                  record.ID,
		Street:              record.Street.String,
		HouseNumber:         record.HouseNumber.Int32,
		HouseNumberAddition: record.HouseNumberAddition.String,
		ZipCode:             record.ZipCode.String,
		Municipality:        model.Municipality{record.MunicipalityCode.String, record.MunicipalityName.String},
		Purpose:             record.Purpose.String,
		Surface:             record.Surface.Int32,
		Buildings:           nil,
	}, nil
}

func ToAddresses(records []*queries.AddressGetRow) ([]model.Address, error) {
	addresses := make([]model.Address, 0, len(records))

	for idx := range records {
		address, err := ToAddress(records[idx])
		if err != nil {
			return nil, err
		}

		addresses = append(addresses, *address)
	}

	return addresses, nil
}

func ToManyAddresses(records []*queries.AddressGetManyRow) ([]*model.Address, error) {
	addresses := make([]*model.Address, len(records))

	var err error
	for i, record := range records {
		if addresses[i], err = ToAddress(&queries.AddressGetRow{
			ID:                  record.ID,
			Street:              record.Street,
			ZipCode:             record.ZipCode,
			MunicipalityCode:    record.MunicipalityCode,
			MunicipalityName:    record.MunicipalityName,
			Purpose:             record.Purpose,
			Surface:             record.Surface,
			HouseNumber:         record.HouseNumber,
			HouseNumberAddition: record.HouseNumberAddition,
			InternalID:          record.InternalID,
		}); err != nil {
			return nil, err
		}
	}

	return addresses, nil
}

func FromAddressCreate(address *model.AddressCreate) *queries.AddressCreateParams {
	return &queries.AddressCreateParams{
		ID:                  address.ID,
		Street:              sql.NullString{String: address.Street, Valid: true},
		HouseNumber:         sql.NullInt32{Int32: address.HouseNumber, Valid: true},
		HouseNumberAddition: sql.NullString{String: address.HouseNumberAddition, Valid: true},
		ZipCode:             sql.NullString{String: address.ZipCode, Valid: true},
		MunicipalityCode:    sql.NullString{String: address.Municipality.Code, Valid: true},
		MunicipalityName:    sql.NullString{String: address.Municipality.Name, Valid: true},
		Purpose:             sql.NullString{String: address.Purpose, Valid: true},
		Surface:             sql.NullInt32{Int32: address.Surface, Valid: true},
	}
}

func FromAddressUpdate(id uuid.UUID, address *model.AddressUpdate) *queries.AddressUpdateParams {
	return &queries.AddressUpdateParams{
		ID:                  id,
		Street:              sql.NullString{String: address.Street, Valid: true},
		HouseNumber:         sql.NullInt32{Int32: address.HouseNumber, Valid: true},
		HouseNumberAddition: sql.NullString{String: address.HouseNumberAddition, Valid: true},
		ZipCode:             sql.NullString{String: address.ZipCode, Valid: true},
		MunicipalityCode:    sql.NullString{String: address.Municipality.Code, Valid: true},
		MunicipalityName:    sql.NullString{String: address.Municipality.Name, Valid: true},
		Purpose:             sql.NullString{String: address.Purpose, Valid: true},
		Surface:             sql.NullInt32{Int32: address.Surface, Valid: true},
	}
}
