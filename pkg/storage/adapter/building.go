package adapter

import (
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/queries/generated"
)

func ToBuilding(record *queries.DigilabDemoFragBuilding) (*model.Building, error) {
	return &model.Building{
		ID:            record.ID,
		ConstructedAt: record.ConstructedAt.Time,
		Surface:       record.Surface.Int32,
		Addresses:     nil,
		Plots:         nil,
	}, nil
}

func ToBuildings(records []*queries.DigilabDemoFragBuilding) ([]model.Building, error) {
	buildings := make([]model.Building, 0, len(records))

	for idx := range records {
		building, err := ToBuilding(records[idx])
		if err != nil {
			return nil, err
		}

		buildings = append(buildings, *building)
	}

	return buildings, nil
}

func FromBuildingCreate(building *model.BuildingCreate) *queries.BuildingCreateParams {
	return &queries.BuildingCreateParams{
		ID:            building.ID,
		ConstructedAt: sql.NullTime{Time: building.ConstructedAt, Valid: true},
		Surface:       sql.NullInt32{Int32: building.Surface, Valid: true},
	}
}

func FromBuildingUpdate(id uuid.UUID, building *model.BuildingUpdate) *queries.BuildingUpdateParams {
	return &queries.BuildingUpdateParams{
		ID:            id,
		ConstructedAt: sql.NullTime{Time: building.ConstructedAt, Valid: true},
		Surface:       sql.NullInt32{Int32: building.Surface, Valid: true},
	}
}
