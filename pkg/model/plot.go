package model

import "github.com/google/uuid"

type PlotsAttachBuildings struct {
	Plots     []uuid.UUID `json:"plots"`
	Buildings []uuid.UUID `json:"buildings"`
}
