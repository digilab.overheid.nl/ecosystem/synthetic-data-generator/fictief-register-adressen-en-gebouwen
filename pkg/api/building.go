//nolint:nlreturn // Makes code more unreadable
package api

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/meta"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/pagination"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/queries/generated"
)

//nolint:dupl // false positive
func (a *API) BuildingList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*queries.DigilabDemoFragBuilding

	switch md.IsASC() {
	case true:
		records, err = a.buildingListAsc(r.Context(), md)
	case false:
		records, err = a.buildingListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	buildings, err := adapter.ToBuildings(records)
	if err != nil {
		writeError(w, err)
		return
	}

	response, err := pagination.Build(buildings, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) buildingListAsc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFragBuilding, error) {
	params := &queries.BuildingListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.BuildingListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("building list failed: %w", err)
	}

	return records, nil
}

func (a *API) buildingListDesc(ctx context.Context, md *meta.Data) ([]*queries.DigilabDemoFragBuilding, error) {
	params := &queries.BuildingListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.BuildingListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("building list failed: %w", err)
	}

	return records, nil
}

func (a *API) BuildingGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	record, err := a.db.Queries.BuildingGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	recordsAddresses, err := a.db.Queries.AddressBuilding(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	recordsPlot, err := a.db.Queries.PlotBuildings(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	building, err := adapter.ToBuilding(record)
	if err != nil {
		writeError(w, err)
		return
	}

	address := make([]*queries.AddressGetRow, 0, len(recordsAddresses))
	for idx := range recordsAddresses {
		address = append(address, (*queries.AddressGetRow)(recordsAddresses[idx]))
	}

	building.Addresses, err = adapter.ToAddresses(address)
	if err != nil {
		writeError(w, err)
		return
	}

	building.Plots = recordsPlot

	if err := json.NewEncoder(w).Encode(building); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) BuildingCreate(w http.ResponseWriter, r *http.Request) {
	building := new(model.BuildingCreate)
	if err := json.NewDecoder(r.Body).Decode(building); err != nil {
		fmt.Print(fmt.Errorf("decode failed: %w", err))
	}

	args := adapter.FromBuildingCreate(building)
	if err := a.db.Queries.BuildingCreate(r.Context(), args); err != nil {
		fmt.Print(fmt.Errorf("building create failed: %w", err))
	}

	w.WriteHeader(http.StatusCreated)
}

func (a *API) BuildingUpdate(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	building := new(model.BuildingUpdate)
	if err := json.NewDecoder(r.Body).Decode(building); err != nil {
		writeError(w, err)
		return
	}

	args := adapter.FromBuildingUpdate(id, building)
	if err := a.db.Queries.BuildingUpdate(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) BuildingDelete(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.BuildingDelete(r.Context(), id); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) BuildingAttachAddresses(w http.ResponseWriter, r *http.Request) {
	attach := new(model.BuildingAttach)
	if err := json.NewDecoder(r.Body).Decode(attach); err != nil {
		writeError(w, err)
		return
	}

	for _, buildingID := range attach.Buildings {
		for _, addressID := range attach.Addresses {
			if err := a.db.Queries.BuildingAttach(r.Context(), &queries.BuildingAttachParams{
				BuildingID: buildingID,
				AddressID:  addressID,
			}); err != nil {
				writeError(w, err)
				return
			}
		}
	}

	w.WriteHeader(http.StatusCreated)
}
