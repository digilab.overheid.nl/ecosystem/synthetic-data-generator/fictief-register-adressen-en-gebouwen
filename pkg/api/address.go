//nolint:nlreturn // Makes code more unreadable
package api

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/meta"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/pagination"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/adapter"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/queries/generated"
)

//nolint:dupl // false positive
func (a *API) AddressList(w http.ResponseWriter, r *http.Request) {
	md, err := meta.New(r)
	if err != nil {
		writeError(w, err)
		return
	}

	var records []*queries.AddressGetRow

	switch md.IsASC() {
	case true:
		records, err = a.addressListAsc(r.Context(), md)
	case false:
		records, err = a.addressListDesc(r.Context(), md)
	}

	if err != nil {
		writeError(w, err)
		return
	}

	addresses, err := adapter.ToAddresses(records)
	if err != nil {
		writeError(w, err)
		return
	}

	response, err := pagination.Build(addresses, md)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) AddressGetMany(w http.ResponseWriter, r *http.Request) {
	var data struct {
		IDs []uuid.UUID `json:"ids"`
	}
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		writeError(w, err)
		return
	}

	if len(data.IDs) > 1000 {
		writeError(w, errors.New("too many IDs requested, should be at most 1000"))
		return
	}

	var addresses []*model.Address

	if len(data.IDs) != 0 {
		records, err := a.db.Queries.AddressGetMany(r.Context(), data.IDs)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				w.WriteHeader(http.StatusNotFound)
				return
			}

			writeError(w, fmt.Errorf("address get many: %w", err))
			return
		}

		if addresses, err = adapter.ToManyAddresses(records); err != nil {
			writeError(w, fmt.Errorf("error converting addresses: %w", err))
			return
		}
	}

	// In case of zero results, return an empty slice instead of null
	if addresses == nil {
		addresses = make([]*model.Address, 0)
	}

	if err := json.NewEncoder(w).Encode(addresses); err != nil {
		writeError(w, fmt.Errorf("encoding failed: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) addressListAsc(ctx context.Context, md *meta.Data) ([]*queries.AddressGetRow, error) {
	params := &queries.AddressListAscParams{
		ID:    md.FirstID,
		Limit: int32(md.PerPage + 1),
	}

	records, err := a.db.Queries.AddressListAsc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("address list failed: %w", err)
	}

	addresses := make([]*queries.AddressGetRow, 0, len(records))
	for idx := range records {
		addresses = append(addresses, (*queries.AddressGetRow)(records[idx]))
	}

	return addresses, nil
}

func (a *API) addressListDesc(ctx context.Context, md *meta.Data) ([]*queries.AddressGetRow, error) {
	search := md.Search
	if search != "" {
		search = fmt.Sprintf("%s:*", search)
	}

	params := &queries.AddressListDescParams{
		ID:         md.LastID,
		InternalID: math.MaxInt32,
		Limit:      int32(md.PerPage + 1),
		ToTsquery:  search,
	}

	records, err := a.db.Queries.AddressListDesc(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("address list failed: %w", err)
	}

	addresses := make([]*queries.AddressGetRow, 0, len(records))
	for idx := range records {
		addresses = append(addresses, &queries.AddressGetRow{
			ID:                  records[idx].ID,
			Street:              records[idx].Street,
			ZipCode:             records[idx].ZipCode,
			MunicipalityCode:    records[idx].MunicipalityCode,
			MunicipalityName:    records[idx].MunicipalityName,
			Purpose:             records[idx].Purpose,
			Surface:             records[idx].Surface,
			HouseNumber:         records[idx].HouseNumber,
			HouseNumberAddition: records[idx].HouseNumberAddition,
		})
	}

	return addresses, nil
}

func (a *API) AddressGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	record, err := a.db.Queries.AddressGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	buildingRecords, err := a.db.Queries.BuildingAddress(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	address, err := adapter.ToAddress(record)
	if err != nil {
		writeError(w, err)
		return
	}

	address.Buildings, err = adapter.ToBuildings(buildingRecords)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(address); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) AddressCreate(w http.ResponseWriter, r *http.Request) {
	address := new(model.AddressCreate)
	if err := json.NewDecoder(r.Body).Decode(address); err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.AddressCreate(r.Context(), adapter.FromAddressCreate(address)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (a *API) AddressUpdate(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	address := new(model.AddressUpdate)
	if err := json.NewDecoder(r.Body).Decode(address); err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.AddressUpdate(r.Context(), adapter.FromAddressUpdate(id, address)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) AddressDelete(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.AddressDelete(r.Context(), id); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
