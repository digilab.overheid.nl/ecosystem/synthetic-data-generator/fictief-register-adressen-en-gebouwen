package api

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func Router(api *API) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(Logger)
	r.Route("/buildings", func(r chi.Router) {
		r.Get("/", api.BuildingList)
		r.Post("/", api.BuildingCreate)

		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", api.BuildingGet)
			r.Put("/", api.BuildingUpdate)
			r.Delete("/", api.BuildingDelete)
		})
		r.Post("/attach/address", api.BuildingAttachAddresses)
	})

	r.Route("/addresses", func(r chi.Router) {
		r.Get("/", api.AddressList)
		r.Post("/", api.AddressCreate)

		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", api.AddressGet)
			r.Put("/", api.AddressUpdate)
			r.Delete("/", api.AddressDelete)
		})
	})
	r.Get("/addresses-many", api.AddressGetMany)
	r.Post("/addresses-many", api.AddressGetMany)

	r.Post("/plots/attach/buildings", api.PlotsAssignBuildings)

	return r
}

func writeError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func Logger(next http.Handler) http.Handler {
	return middleware.RequestLogger(&logFormatter{Logger: log.New(os.Stdout, "", log.LstdFlags)})(next)
}

type logFormatter struct {
	Logger middleware.LoggerInterface
}

type logEntry struct {
	Logger  middleware.LoggerInterface
	request *http.Request
	buf     *bytes.Buffer
}

func (l *logEntry) Write(status, amount int, _ http.Header, elapsed time.Duration, _ interface{}) {
	fmt.Fprintf(l.buf, "%03d %dB in %s", status, amount, elapsed)
	l.Logger.Print(l.buf.String())
}

func (l *logEntry) Panic(v interface{}, _ []byte) {
	middleware.PrintPrettyStack(v)
}

func (l *logFormatter) NewLogEntry(r *http.Request) middleware.LogEntry { //nolint:ireturn // Need to return interface for impl of interface
	entry := &logEntry{
		Logger:  l.Logger,
		request: r,
		buf:     &bytes.Buffer{},
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}

	fmt.Fprintf(entry.buf, "%s://%s%s %s\" ", scheme, r.Host, r.RequestURI, r.Proto)

	return entry
}
