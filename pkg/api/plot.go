//nolint:nlreturn,wsl // Makes code more unreadable
package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-backend/pkg/storage/queries/generated"
)

func (a *API) PlotsAssignBuildings(w http.ResponseWriter, r *http.Request) {
	item := new(model.PlotsAttachBuildings)
	if err := json.NewDecoder(r.Body).Decode(item); err != nil {
		writeError(w, err)
		return
	}

	tx, err := a.db.DB.BeginTx(r.Context(), nil)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := func() error {
		for _, plotID := range item.Plots {
			for _, buildingID := range item.Buildings {
				arg := &queries.PlotAttachParams{
					PlotID:     plotID,
					BuildingID: buildingID,
				}

				if err := a.db.Queries.WithTx(tx).PlotAttach(r.Context(), arg); err != nil {
					return fmt.Errorf("plot attach failed: %w", err)
				}
			}
		}
		return nil
	}(); err != nil {
		_ = tx.Rollback()

		writeError(w, err)
		return
	}

	if err := tx.Commit(); err != nil {
		writeError(w, err)
		return
	}
}
