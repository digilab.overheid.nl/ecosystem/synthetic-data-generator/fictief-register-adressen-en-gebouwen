---
title : "Fictief register adressen en gebouwen"
description: "Documentation for the address and building records database"
lead: ""
date: 2023-08-022T14:52:40+02:00
draft: true
toc: true
---

## Running locally
Clone [this repo](https://gitlab.digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen).

# Running in docker
To run the development server for the first time in docker on port 9000:

Run:

```shell
make build

make up
```

To stop the development server

Run:

```shell
make down
```

To view the logs of the development server

Run:

```shell
make logs
```
